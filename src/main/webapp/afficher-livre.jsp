<%@ page language="java" contentType="text/html; charset=utf-8"
	pageEncoding="utf-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

<!-- Cette page ne sert à rien pour l'instant mais elle pourrait permettre d'afficher les détails d'un livre (résumé, disponibilités...) -->

<!DOCTYPE html>
<html>
	<head>
		<meta charset="UTF-8">
		<title>Infos livre</title>
		<link type="text/css" rel="stylesheet" href="inc/style.css" />
	</head>
	<body>
		<p class="info">${ message }</p>
		<fieldset>
			<legend>Livre</legend>
			<p>Titre : ${ book.title }</p>
			<p>Auteur : ${ book.authorname }</p>
			<p>Année : ${ book.year }</p>
			<p>Éditeur : ${ book.publisher }</p>
		</fieldset>
	</body>
</html>
<%@ page language="java" contentType="text/html; charset=utf-8"
	pageEncoding="utf-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<link rel="stylesheet" href="css/bootstrap.min.css">
<script src="js/jquery.js"></script>
<title>Tous les livres</title>
</head>

<body id="page">
	<div class="container">
		<form action="selectBooks">
			<h3>Livres disponibles :</h3>
			<br>
			<div class="container">
				<div class="row" id="ligne1">
					<div class="col-sm-1 border border-primary rounded">
						<span>ID</span>
					</div>
					<div class="col-sm-3 border border-primary rounded">
						<span>Titre</span>
					</div>
					<div class="col-sm-2 border border-info rounded">
						<span>Auteur</span>
					</div>
					<div class="col-sm-1 border border-warning rounded">
						<span>Année</span>
					</div>
					<div class="col-sm-2 border border-info rounded">
						<span>Éditeur</span>
					</div>
				</div>
				<br>
				<c:forEach items="<%=(new principal.LibraryController()).getBooks()%>" var="b">
					<div class="row border border-info-dark rounded" id="ligne">
						<div class="form-check">
							<input type="checkbox" name="livre" value="${b.id}" onchange="document.getElementById('selectionner').disabled = !this.checked;">
						</div>
							<div class="col-sm-1 border border-primary rounded">
								<span><c:out value="${b.id}" /></span>
							</div>
							<div class="col-sm-2 border border-primary rounded">
								<span><c:out value="${b.title}" /></span>
							</div>
							<div class="col-sm-2 border border-info rounded">
								<span id="authorfirstname"> <c:out
										value="${b.authorfirstname}" /></span> <span id="authorname"><c:out
										value="${b.authorname}" /></span>
							</div>
							<div class="col-sm-1 border border-warning rounded">
								<span id="year"><c:out value="${b.year}" /></span>
							</div>
							<div class="col-sm-2 border border-info rounded">
								<span id="publisher"><c:out value="${b.publisher}" /></span>
							</div>
					</div>
					<br>
				</c:forEach>
				</div>
			<input id="selectionner" type="submit" value="Sélectionner"/>
		</form>
		<br>
		<h3><a href="/index.html">ACCUEIL</a></h3>
	</div>
</body>

</html>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<link rel="stylesheet" href="css/bootstrap.min.css">
<script src="js/jquery.js"></script>
<title>Tous les livres</title>
</head>

<body id="page">
	<div class="container">
		<h2>~ TOUS LES LIVRES ~</h2>
		<br>
		<table class="table table-striped">
		    <thead>
		      <tr>
		        <th>TITRE</th>
		        <th>AUTEUR</th>
		        <th>ANN�E</th>
		        <th>�DITEUR</th>
		      </tr>
		    </thead>
		    <tbody>
		    	<c:forEach items="<%=(new principal.LibraryController()).getBooks()%>" var="b">
		    		<tr>
				        <td><c:out value="${b.title}" /></td>
				        <td><c:out value="${String.format(\"%s %s\", b.authorfirstname, b.authorname)}" /></td>
				        <td><c:out value="${b.year}" /></td>
				        <td><c:out value="${b.publisher}" /></td>
				      </tr>
		    	</c:forEach>
		    </tbody>
		  </table>
		  <br>
		  <h3><a href="/index.html">ACCUEIL</a></h3>
	</div>
</body>

</html>
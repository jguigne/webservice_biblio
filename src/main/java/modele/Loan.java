package modele;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Un emprunt
 */
public class Loan {
	
	// For compatibility (SQLite), date are saved as strings! 
	public static final String DATE_FORMAT = "yyyy-MM-dd"; 
	
	private int id;
	private int bookId;
	private String user;
	private Date startDate;
	private Date endDate;
	
	public Loan(int bookId, String user) {
		this(0, bookId, user, new Date(), null);
	}
	
	public Loan(int id, int bookId, String user, Date startDate, Date endDate) {
		super();
		this.id = id;
		this.bookId = bookId;
		this.user = user;
		this.startDate = startDate;
		this.endDate = endDate;
	}

	@Override
	public String toString() {
		DateFormat df = new SimpleDateFormat(DATE_FORMAT);
		String startDateStr = "?";
		if(startDate!=null)
			startDateStr = df.format(startDate);
		String endDateStr = "?";
		if(endDate !=null)
			endDateStr = df.format(endDate);
		return startDateStr+" - "+endDateStr+", "+user+ " : "+bookId;
	}
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public int getBookId() {
		return bookId;
	}
	public void setBookId(int bookId) {
		this.bookId = bookId;
	}
	public String getUser() {
		return user;
	}
	public void setUser(String user) {
		this.user = user;
	}

	public Date getStartDate() {
		return startDate;
	}

	public void setStartDate(Date startDate) {
		this.startDate = startDate;
	}

	public Date getEndDate() {
		return endDate;
	}

	public void setEndDate(Date endDate) {
		this.endDate = endDate;
	}
	
}

package modele;

/**
 * Un livre
 */
public class Book {
	private int id;
	private String title;
	private String publisher;
	private int year;
	private String authorname;
	private String authorfirstname;
	
	public Book() {
		
	}
	
	public Book(int id, String title, String publisher, int year, String authorname, String authorfirstname) {
		super();
		this.id = id;
		this.title = title;
		this.publisher = publisher;
		this.year = year;
		this.authorname = authorname;
		this.authorfirstname = authorfirstname;
	}

	public Book(String title, String publisher, int year, String authorname, String authorfirstname) {
		this(0, title, publisher, year, authorname, authorfirstname);
	}
	
	@Override
	public String toString() {
		return String.format(" > %s \n > Auteur : %s %s \n > Année : (%d) \n > Édition : %s", 
				title, authorfirstname, authorname, year, publisher);
	}
	
	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public String getPublisher() {
		return publisher;
	}
	public void setPublisher(String publisher) {
		this.publisher = publisher;
	}
	public int getYear() {
		return year;
	}
	public void setYear(int year) {
		this.year = year;
	}
	public String getAuthorname() {
		return authorname;
	}
	public void setAuthorname(String authorname) {
		this.authorname = authorname;
	}
	public String getAuthorfirstname() {
		return authorfirstname;
	}
	public void setAuthorfirstname(String authorfirstname) {
		this.authorfirstname = authorfirstname;
	}
}

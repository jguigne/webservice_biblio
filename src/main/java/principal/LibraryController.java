package principal;

import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Scanner;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

import DAO.BooksDAO;
import modele.Book;

@RestController
public class LibraryController extends HttpServlet {

	private static final long serialVersionUID = 1L;
	private BooksDAO dao;
	private final String adresseVoisin = "http://192.168.1.27:8080/biblio/";

	//=========================================================================================================================================
	
	/*
	 * Méthodes de conversion d'un objet JSON en objet Book
	 * Par la suite, s'orienter dans la mesure du possible vers des méthodes générales et évolutives du genre :
	 * 
			 public Book objetGet(JsonObject objet,	List<Class> typesFournis, List<String> parametresRequis) {						
				 	List<Object> pl = new ArrayList<Object>();
				 	
				 	for (String p:parametresRequis) {
				 		pl.add(objet.get(p).getAsX());
				 	}
				 	
				 	return new Book( (typesFournis) pl );
			 }
		
	 * A voir ...
	 */
	public Book objetGet(JsonObject objet,
							String id,
							String titre,
							String editeur,
							String annee,
							String authorName,
							String authorFirstName) {
		return new Book(
				Integer.parseInt(objet.get(id).getAsString()),
				objet.get(titre).getAsString(),
				objet.get(editeur).getAsString(),
				Integer.parseInt(objet.get(annee).getAsString()),
				objet.get(authorName).getAsString(),
				objet.get(authorFirstName).getAsString());
	}
	
	// Méthode utilisée dans le traitement du formulaire de book-form.jsp (livres du voisin)
	public Book objetGetVoisin(JsonObject objet,
								String id,
								String titre,
								String editeur,
								String annee,
								String authorName,
								String authorFirstName) {
		return new Book(
				objet.get(id).getAsInt(),
				objet.get(titre).getAsString(),
				objet.get(editeur).getAsString(),
				objet.get(annee).getAsInt(),
				objet.get(authorName).getAsString(),
				objet.get(authorFirstName).getAsString());
	}

	//=========================================================================================================================================
	
	/*
	 * Enregistrement d'un nouveau livre à partir du formulaire en page d'index
	 */
	@GetMapping(value = "/creationLivre")
	public void insertBook(HttpServletRequest request) throws SQLException, ClassNotFoundException, IOException, ServletException {
		/* Récupération des paramètres de requête */
		System.out.println("ok1");
		String title = request.getParameter("titre");
		String publisher = request.getParameter("publisher");
		int year = Integer.valueOf(request.getParameter("year"));
		String authorname = request.getParameter("authorname");
		String authorfirstname = request.getParameter("authorfirstname");
		
		System.out.println("ok2");
		/* Création du bean */
		Book b = new Book(title, publisher, year, authorname, authorfirstname);
		/* Enregistrement en BDD */
		BooksDAO dao;
		try {
			dao = new BooksDAO();
			dao.createBook(b);
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		System.out.println(b);
		// TODO >> Rediriger vers la page d'accueil
	}

	/*
	 * Retourne l'intégralité des livres de notre base de données de livres
	 */
	@GetMapping(value = "/books", produces = "application/json")
	public List<Book> getBooks() throws SQLException, ClassNotFoundException, IOException {
		dao = new BooksDAO();
		return dao.getBooks();
	}
	
	/*
	 * Retourne le nombre de livres présents en base de données de livres
	 * (des méthodes similaires peuvent être implémentées pour récupérer d'autres données (tris, identifiants...)
	 */
	@GetMapping(value = "/howManyBooks")
	public int getHowManyBooks() throws ClassNotFoundException, SQLException, IOException {
		return getBooks().size();
	}
	
	/*
	 * Renvoie le livre d'identifiant n s'il est disponible (getBooks() pourra toujours être adapter à de nouvelles conditions de disponibilité...)
	 */
	@GetMapping(value = "/books/{n}", produces = "application/json")
	public Book getBook(@PathVariable("n") int n) throws SQLException, ClassNotFoundException, IOException, IndexOutOfBoundsException {
		try {
			Book b = getBooks().get(n-1);
			System.out.println(b + " supprimé de la base ! ");
			dao = new BooksDAO();
			dao.deleteBook(b);
			return b;
		} catch (Exception ex) {
			System.out.println("Le livre à l'index " + n + " nest pas disponible...");
			return null;
		}
	}

	/*
	 * Méthode qui récupère les livres choisis dans le formulaire de sélection correspondant (book-form.jsp)
	 * Pour chaque item sélectionné, on effectue une requête GET chez le voisin afin de le retirer de sa base de données de livres
	 */
	@GetMapping(value = "/selectBooksVoisin")
	public void selectionBookVoisinAndRemoteDelete(HttpServletRequest request) throws IOException, ClassNotFoundException, SQLException, ServletException {		
		
		BooksDAO dao = new BooksDAO();
		
		// Parcours des items sélectionnés
		for (String v: request.getParameterValues("livre")) {
			// on récupère l'identifiant
			int id = Integer.valueOf(v);
			// on forme l'URL correspondante et on se connecte
			URL url = new URL(adresseVoisin + id);
			HttpURLConnection conn = (HttpURLConnection) url.openConnection();
			// !! INSTANCIER ICI L'OBJET BOOK CORRESPONDANT !! (à vérifier...)
			Book b = (Book) conn.getContent();
			// compléter BDD
			dao.createBook(b);
			// déconnexion
			conn.disconnect();
		}
	}
	
	/*
	 * Méthode de conversion d'un objet JSON en liste d'objets Book
	 * Cette liste est appelée dans la page book-form.jsp afin de générer le formulaire HTML
	 */
	public List<Book> getBooksVoisin() throws SQLException, ClassNotFoundException, IOException {
		URL urlVoisin = new URL(adresseVoisin);

		HttpURLConnection conn = (HttpURLConnection) urlVoisin.openConnection();
		conn.setRequestMethod("GET");
		conn.connect();

		Scanner sc = new Scanner(urlVoisin.openStream());
		String json = "";
		while (sc.hasNext()) {
			json += sc.nextLine();
		}
		System.out.println(json);

		JsonArray ja = new JsonParser().parse(json).getAsJsonArray();
		List<Book> lb = new ArrayList<Book>();

		for (JsonElement jo : ja) {
			Book b = objetGetVoisin((JsonObject) jo, "id", "titre", "editeur", "annee", "nomAuteur", "prennomAuteur");
			lb.add(b);
		}
		conn.disconnect();
		return lb;
	}
	
	//=========================================================================================================================================
	
	/*
	 * Bac à sable ...
	 * Méthode de test de récupération des données du formulaire de book-form-bis.jsp
	 */
	@GetMapping(value = "/selectBooks")
	public void getBookFormulaire(HttpServletRequest request) throws SQLException, ClassNotFoundException, IOException, ServletException {
		/* Récupération des paramètres de requête */
		for (String id_txt : request.getParameterValues("livre")) {
			System.out.println(id_txt);
			int id = Integer.valueOf(id_txt);
			
			dao = new BooksDAO();
			// Book book = dao.getBookById(id);
			// System.out.println(book);
			
			// Rediriger vers la page d'accueil
		}
		
	}

}

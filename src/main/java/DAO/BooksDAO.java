package DAO;

import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import modele.Book;
import modele.Loan;

/**
 * le DAO pour l'ensemble des données
 */
public class BooksDAO {
	
	private Connection conn;
	
	public BooksDAO() throws IOException, SQLException, ClassNotFoundException {
		Class.forName("org.sqlite.JDBC");
		conn = DriverManager.getConnection("jdbc:sqlite:books.sqlite");
		createTables();
	}
	
	public void close() throws SQLException {
		conn.close();
	}
	
	/**
	 * Enregistre un livre
	 * @param b le nouveau livre
	 * @throws SQLException
	 */
	public void createBook(Book b) throws SQLException {
		PreparedStatement ps = conn.prepareStatement(
				"insert into books(title, publisher, year, authorname, authorfirstname)"+
				"values(?,?,?,?,?)"
		);
		ps.setString(1, b.getTitle());
		ps.setString(2, b.getPublisher());
		ps.setInt(3, b.getYear());
		ps.setString(4, b.getAuthorname());
		ps.setString(5, b.getAuthorfirstname());
		ps.execute();
	}
	/**
	 * Supprime un livre
	 * @param b le livre
	 * @throws SQLException
	 */
	public void deleteBook(Book b) throws SQLException {
		if (b.getId() > 0) {
			PreparedStatement ps = conn.prepareStatement(
					"delete from books where id = ?"				
			);
			ps.setInt(1, b.getId());
			ps.execute();
		}
	}
	/**
	 * 
	 * @return tous les livres
	 * @throws SQLException
	 */
	public List<Book> getBooks() throws SQLException {
		List<Book> res = new ArrayList<Book>();
		ResultSet rs = conn.createStatement().executeQuery(
				"select * from books order by authorname,authorfirstname,year"
		);
		while(rs.next()) {
			res.add(new Book(
					rs.getInt("id"),
					rs.getString("title"), 
					rs.getString("publisher"),
					rs.getInt("year"),
					rs.getString("authorname"),
					rs.getString("authorfirstname")));
		}
		rs.close();
		return res;
	}
	
	/**
	 * Enregistre un emprunt de livre
	 * @param l le nouvel emprunt
	 * @throws SQLException
	 */
	public void createLoan(Loan l) throws SQLException {
		PreparedStatement ps = conn.prepareStatement(
				"insert into loans(user, book, startdate)"+
				"values(?,?,?)"
		);
		ps.setString(1, l.getUser());
		ps.setInt(2, l.getBookId());
		ps.setString(3, new SimpleDateFormat(Loan.DATE_FORMAT).format(new Date()));
		ps.execute();
	}
	
	/**
	 * Renvoie tous les emprunts
	 * @return tous les emprunt, les non-rendus d'abord
	 * @throws SQLException
	 * @throws ParseException
	 */
	public List<Loan> getLoans() throws SQLException, ParseException {
		List<Loan> res = new ArrayList<Loan>();
		ResultSet rs = conn.createStatement().executeQuery(
				"select * from loans order by enddate, startdate"
		);
		DateFormat df = new SimpleDateFormat(Loan.DATE_FORMAT);
		while(rs.next()) {
			Date startDate = null;
			if(rs.getString("startdate")!=null)
				startDate = df.parse(rs.getString("startdate"));
			Date endDate = null;
			if(rs.getString("enddate")!=null)
				endDate = df.parse(rs.getString("enddate"));			
			res.add(new Loan(
					rs.getInt("id"),
					rs.getInt("book"), 
					rs.getString("user"),
					startDate,
					endDate));
		}
		rs.close();
		return res;
	}
	
	/**
	 * Termine un emprunt (le livre est rendu) 
	 * @param loanId identifiant de l'emprunt
	 * @throws SQLException
	 */
	public void endLoan(int loanId) throws SQLException {
		DateFormat df = new SimpleDateFormat(Loan.DATE_FORMAT);
		PreparedStatement ps = conn.prepareStatement("update loans set enddate=? where id=?");
		ps.setString(1, df.format(new Date()));
		ps.setInt(2, loanId);
		ps.execute();
	}
	
	/**
	 * Crée, si nécessaire, toutes les tables
	 * @throws SQLException
	 */
	private void createTables() throws SQLException {
		conn.createStatement().execute(
				"create table if not exists books(id integer primary key, "+
						"title varchar(100), "+
						"publisher varchar(100), "+
						"year integer, "+
						"authorname varchar(100), "+
						"authorfirstname varchar(100));");
		
		conn.createStatement().execute(
				"create table if not exists loans(id integer primary key, "+
						"book integer, "+
						"user varchar(100), "+
						"startdate varchar(32), "+
						"enddate varchar(32));");
	}
	
}

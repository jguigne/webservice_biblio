package controleurs;

import java.io.IOException;
import java.sql.SQLException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import DAO.BooksDAO;
import modele.Book;

/**
 * Servlet implementation class CreationLivre
 */
@WebServlet(value = "/CreationLivre", urlPatterns = {"/creationLivre"})
public class CreationLivre extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public CreationLivre() {
        super();
    }

	/**
	 * @see HttpServlet#service(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void service(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		/* Récupération des paramètres de requête */
		String title = request.getParameter( "titre" );
		String publisher = request.getParameter( "publisher" );
		int year = Integer.valueOf(request.getParameter( "year" ));
		String authorname = request.getParameter( "authorname" );
		String authorfirstname = request.getParameter( "authorfirstname" );
		/* Création du bean */
	    Book b = new Book(title, publisher, year, authorname, authorfirstname);
	    
	    /* Enregistrement en BDD */
		BooksDAO dao;
		try {
			dao = new BooksDAO();
			dao.createBook(b);
		} catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		/* Redirection */
		System.out.println(b);
	    String message;
	    Object[] requis = {title, authorname};
	    boolean valide = true;
	    for (Object item:requis) {
	    	valide = valide && (!item.toString().equals(""));
	    }
	    if (valide) {
	    	message = "Livre enregistré avec succès";
	    } else {
	    	String redirect = getServletContext().getContextPath()+ "/index.html";
	    	message = "Erreur - Vous n'avez pas rempli tous les champs obligatoires. <br>";
	    	message += "<a href=\""+redirect+"\">Cliquez ici</a> pour retourner au formulaire.";
	    }
	    request.setAttribute( "book", b);
	    request.setAttribute( "message", message);
	    /* Envoi de la réponse */
	    this.getServletContext().getRequestDispatcher( "/afficher-livre.jsp" ).forward( request, response );
	}

}
